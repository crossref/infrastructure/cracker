import os
import secrets
from typing import Dict, List

from fastapi import FastAPI
from pydantic import BaseModel

SHARED_SECRET = os.environ.get("SHARED_SECRET")

class CreateSlackBot(BaseModel):
    secret: str
    channel_name: str
    bot_name: str
    icon: str

class SendSlackMessage(BaseModel):
    token: str
    message: str

app = FastAPI(
    title="",
    description="",
    # maybe this version could be generated from gitpython?
    version="1.0.0",
    # The openapi schema is usually served from /openapi.json
    # You can change the url or disable it with openapi_url
    # openapi_url=None,
    # Swagger UI: served at /docs.
    # You can set its URL with the parameter docs_url.
    # You can disable it by setting docs_url=None.
    # ReDoc: served at /redoc.
    # You can set its URL with the parameter redoc_url.
    # You can disable it by setting redoc_url=None.
    # docs_url="/docs",
    #redoc_url=None,
)

# Create bot
# get list of channels, does channel exist.
# is valid bot name
# is valid icon/emoji # no api, but can find on https://www.webfx.com/tools/emoji-cheat-sheet/
# create token
# download file from s3
# write new token etc. to file
# upload file to s3

@app.get("/")
async def root() -> Dict[str, str]:
    return {"message": "Hello World!"}

# list all the current bots, maybe without tokens?
# require shared secret
@app.get("/slack")
async def get_bot(bot):
        return {"message": ""}

@app.post("/slack")
async def create_bot(bot: CreateSlackBot):
    if secrets.compare_digest(bot.secret, SHARED_SECRET):
        token = secrets.token_hex(16)
        store_in_db(token)
        return {"message": token}
    else:
        return {"message": "not worked"}

@app.post("/slack/message")
async def send_message(message: SendSlackMessage):
    if secrets.compare_digest(message.token, check_db_for_token()):
        return {"message": "it worked"}
    else:
        return {"message": "not worked"}

@app.delete("/slack")
async def delete_bot(bot):
        return {"message": ""}

#@app.post(
#    "/notes/",
#    response_model=Note,
#    summary="summary in decorator. Lets create some notes",
#    response_description="description used in response in swagger UI",
#    tags=["notes"],
#)
#async def create_note(note: Note):
#    """
#    # Example documentation
#
#    Create notes for a todo list
#
#    This docstring is added as documentation in the swagger UI for this function. It renders markdown so you can do things like **bold**.
#
#    Response model = [response_model](https://fastapi.tiangolo.com/tutorial/response-model/)
#
#    The summary is usually just the function name with `_` replaced with spaces and each word capitalised. But you can override it with the summary parameter.
#
#    response_description is used to document the response in the swagger UI
#
#    Tags group these functions together in the swagger UI
#    """
#    return note
