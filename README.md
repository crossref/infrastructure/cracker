# Cracker

Before doing anything here, read https://api.slack.com/messaging/webhooks#getting_started and see if incoming webhooks are actually already in slack just need a specific app creating
Or otherwise you'll be using this: https://api.slack.com/messaging/webhooks#incoming_webhooks_programmatic for sending





Send HTTP requests and have messages appear in slack. Similar to original slack incoming webhook.

Use SQLmodel + sqlite + litestream
<https://sqlmodel.tiangolo.com/tutorial/fastapi/multiple-models/>

Would it be better/easier to just container in fargate than lambda mess about?
0.25CPU is $10 per month

- Read "Bot User OAuth Token" and "Signing Secret" from env var. Used to auth to slack API

- FastAPI
- Uses [Bolt](https://slack.dev/bolt-python/tutorial/getting-started)
- Has access to all channels but needs to be told where to send to
- Send POST request with preshared secret string to create new channel integration with specific name (or maybe a token?)
    - this would set the name, avatar etc.
    - When you want to send a message, you use this token so that the avatar etc. are set automatically
    - would be cracker.crossref.org/create/ with JSON body for details and header or body for secret
- Send POST/PUT with message body to specific ^ name/token which will cause cracker to send slack message.
    - would be cracker.crossref.org/send/$token with JSON body for message


GET to /slack
List all created integrations

GET to /slack/channels
List all slack channels in crossref workspace

POST to /slack
Body of preshared secret, channel name, username, icon
Returns a secret token

POST to /slack/message
Body of secret token, message

DELETE to /slack
Body of preshared secret, channel name, username, icon
Removes the integration
Returns success/fail

Restrict icon to icon_emoji only to stop impersonation of real users
